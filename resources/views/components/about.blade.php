<!-- feature section start -->
<section class="feature-area" id="koin">
    <div class="container">
        <div class="row flexbox-center">
            <div class="col-lg-6">
                <div class="single-feature-box">
                    <img src="{{ url('/') }}/img/img-1.jpg" alt="feature" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="single-feature-box">
                    <p>Satria Dewa Gatotkaca dan Cap Badak mempersembahkan 10 koleksi Koin Misteri Gatotkaca yang didesain sesuai atribut khas Gatotkaca dengan material terbaik.</p>
                </div>
            </div>
        </div>
        <div class="row flexbox-center">
            <div class="col-lg-6 grid1-order">
                <div class="single-feature-box">
                    <p>Seluruh jenis Koin Gatotkaca dapat ditukar jadi nilai ratusan hingga jutaan rupiah. Hanya beli paket Bundle isi 6 pcs kaleng Larutan Penyegar Cap Badak yang sudah berisi Koin Gatotkaca, Anda bisa menangi hadiahnya!</p>
                </div>
            </div>
            <div class="col-lg-6 grid2-order">
                <div class="single-feature-box">
                    <img src="{{ url('/') }}/img/img-2.jpg" alt="feature" />
                </div>
            </div>
        </div>
    </div>
</section><!-- feature section end -->

<section class="about-area ptb-100" id="fungsi" style="color:white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title">
                    <h2>Apa Saja Fungsi Dari Koin Gatotkaca?</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="single-about-box">
                    {{-- <i class="icofont icofont-file-document"></i> --}}
                    <img src="{{ url('/') }}/img/collectible.png" alt="feature" width="100px"  />
                    <h4>Collectible Item</h4>
                    <p>Bagi Anda yang hobi mengoleksi barang unik, 10 jenis Koin Gatotkaca yang kece ini udah pasti gak boleh sampai ketinggalan!</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="single-about-box">
                    {{-- <i class="icofont icofont-rocket"></i> --}}
                    <img src="{{ url('/') }}/img/tukar-uang.png" alt="feature" width="100px"  />
                    <h4>Ditukar Hadiah Uang</h4>
                    <p>Setiap jenis Koin Gatotkaca bisa Anda tukar dengan hadiah ratusan hingga jutaan rupiah.</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="single-about-box">
                    {{-- <i class="icofont icofont-tools-alt-2"></i> --}}
                    <img src="{{ url('/') }}/img/collector-kit.png" alt="feature" width="100px"  />
                    <h4>Ditukar Collector Kit</h4>
                    <p>Collector Kit Koin Gatotkaca yang mewah dan Limited Edition juga bisa Anda bawa pulang kok!</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="single-about-box">
                    {{-- <i class="icofont icofont-tools-alt-2"></i> --}}
                    <img src="{{ url('/') }}/img/meet-n-greet.png" alt="feature" width="100px"  />
                    <h4>Bertemu Cast Gatotkaca</h4>
                    <p>Kesempatan emas untuk bertemu secara online dengan cast Film Satria Dewa Gatotkaca gak boleh Anda sia-siakan!</p>
                </div>
            </div>
        </div>
    </div>
</section><!-- about section end -->

