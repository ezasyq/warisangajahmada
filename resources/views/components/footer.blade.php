<!-- footer section start -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="footer-content">
                    <p>Copyright Warisan Gajahmada - All Right Reserved</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footer-content">
                    <ul>
                        <li><a href="#home">Home</a></li>
                        <li><a href="#koin">Koin Gatotkaca</a></li>
                        <li><a href="#fungsi">Fungsi</a></li>
                        <li><a href="#caratukar">Cara Tukar</a></li>
                        <li><a href="#order">Order</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!-- footer section end -->